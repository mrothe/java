# Copyright 2009-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2009 Sterling X. Winter <replica@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 1.13 1.12 1.11 1.10 ] ] \
        java [ virtual_jdk_and_jre_deps=false ] \
        java-jdk \
        flag-o-matic

export_exlib_phases pkg_setup src_unpack src_configure src_compile src_test_expensive src_install pkg_preinst

SUMMARY="IcedTea allows for building OpenJDK without any non-free components"
DESCRIPTION="
The IcedTea project provides a harness to build the source code from
http://openjdk.java.net using Free Software build tools and adds a number of
key features to the upstream OpenJDK codebase:
  * A Free 64-bit plugin with LiveConnect and Java Web Start support (now in dev-lang/icedtea-web)
  * Support for additional platforms via a pure interpreted mode in HotSpot
    (Zero) or the alternative CACAO virtual machine. Experimental JIT support
    for Zero is also available via Shark.
"
HOMEPAGE="http://icedtea.classpath.org"
BUGS_TO="philantrop@exherbo.org"

LICENCES="GPL-2"

DEPENDENCIES="
    build:
        app-arch/unzip
        app-arch/zip
    build+run:
        app-misc/ca-certificates[>=20170717-r1][ca-trust]
        dev-libs/atk
        dev-libs/glib:2[>=2.26]
        dev-libs/nss[>=3.12.6]
        media-libs/fontconfig
        media-libs/freetype:2[>=2.2.1]
        media-libs/giflib:=
        media-libs/lcms2[>=2.5]
        media-libs/libpng:=
        net-print/cups
        sys-sound/alsa-lib
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:=
        x11-libs/libX11
        x11-libs/libXext[>=1.1.1]
        x11-libs/libXi
        x11-libs/libXinerama
        x11-libs/libXp
        x11-libs/libXrender
        x11-libs/libXt
        x11-libs/libXtst
        x11-libs/pango
        providers:heimdal? ( app-crypt/heimdal )
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=7] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:krb5? ( app-crypt/krb5 )
    recommendation:
        dev-java/apache-ant[>=1.10.0]
    suggestion:
        dev-lang/icedtea-sound[>=1.0.1] [[ description = [ javax.sound plugins for any JDK ] ]]
        dev-lang/icedtea-web [[ description = [ Java browser plugin & Java Web Start ] ]]
"

MYOPTIONS="
    doc
    examples
    ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( platform: amd64 x86 )
"

if [[ -n ${REVISION} ]]; then
    BASE_URI="http://icedtea.classpath.org/hg/${PN}-forest"
else
    BASE_URI="http://icedtea.wildebeest.org/download/drops/${PN}/$(ever range 1-3)"
fi

ARC_EXT=xz

DOWNLOADS="
    ${BASE_URI}/openjdk.tar.${ARC_EXT} -> openjdk-${SLOT}-${PV}.tar.${ARC_EXT}
    ${BASE_URI}/corba.tar.${ARC_EXT} -> corba-${PV}.tar.${ARC_EXT}
    ${BASE_URI}/jaxp.tar.${ARC_EXT} -> jaxp-${PV}.tar.${ARC_EXT}
    ${BASE_URI}/jaxws.tar.${ARC_EXT} -> jaxws-${PV}.tar.${ARC_EXT}
    ${BASE_URI}/jdk.tar.${ARC_EXT} -> jdk-${PV}.tar.${ARC_EXT}
    ${BASE_URI}/langtools.tar.${ARC_EXT} -> langtools-${PV}.tar.${ARC_EXT}
    ${BASE_URI}/hotspot.tar.${ARC_EXT} -> hotspot-${PV}.tar.${ARC_EXT}
    ${BASE_URI}/nashorn.tar.${ARC_EXT} -> nashorn-${PV}.tar.${ARC_EXT}

    platform:amd64? (
        http://dev.exherbo.org/~philantrop/distfiles/icedtea7-bin-${BOOTSTRAP_VERSION}-amd64.tar.xz -> ${PN}-bin-${BOOTSTRAP_VERSION}-amd64.tar.xz
        http://dev.exherbo.org/~philantrop/distfiles/apache-ant-bin-${BOOTSTRAP_ANT_VERSION}-amd64.tar.bz2
    )
    platform:x86? (
        http://dev.exherbo.org/~philantrop/distfiles/icedtea7-bin-${BOOTSTRAP_VERSION}-x86.tar.xz -> ${PN}-bin-${BOOTSTRAP_VERSION}-x86.tar.xz
        http://dev.exherbo.org/~philantrop/distfiles/apache-ant-bin-${BOOTSTRAP_ANT_VERSION}-x86.tar.bz2
    )
"

if [[ -n ${REVISION} ]]; then
    DOWNLOADS+="http://dev.exherbo.org/~philantrop/distfiles/${PNV}-${REVISION}.tar.xz"
    WORK=${WORKBASE}/${PNV}-${REVISION}
else
    DOWNLOADS+="${HOMEPAGE}/download/source/icedtea-${PV/_pre1/pre}.tar.xz -> ${PNV}.tar.xz"
fi

WORK=${WORKBASE}/icedtea-${PV/_pre1/pre}

_icedtea_option() {
    local myoption=${1} mapping=${2}
    if option ${myoption} ; then
        if [[ -n ${mapping} ]]; then
            echo "--enable-${mapping}"
        else
            echo "--enable-${myoption}"
        fi
    else
        if [[ -n ${mapping} ]]; then
            echo "--disable-${mapping}"
        else
            echo "--disable-${myoption}"
        fi
    fi
}

icedtea_pkg_setup() {
    if ! has_version dev-lang/icedtea8 || ! has_version dev-java/apache-ant ; then
        MUST_BOOTSTRAP=1
        einfo "Auto-bootstrapping ${PN}"
    else
        unset MUST_BOOTSTRAP
    fi
}

icedtea_src_unpack() {
    if [[ -n ${REVISION} ]]; then
        unpack ${PNV}-${REVISION}.tar.xz
    else
        unpack ${PNV}.tar.xz
    fi

    if option platform:amd64 && [[ -n ${MUST_BOOTSTRAP} ]] ; then
        unpack ${PN}-bin-${BOOTSTRAP_VERSION}-amd64.tar.xz
        unpack apache-ant-bin-${BOOTSTRAP_ANT_VERSION}-amd64.tar.bz2
    elif option platform:x86 && [[ -n ${MUST_BOOTSTRAP} ]] ; then
        unpack ${PN}-bin-${BOOTSTRAP_VERSION}-x86.tar.xz
        unpack apache-ant-bin-${BOOTSTRAP_ANT_VERSION}-x86.tar.bz2
    fi
}

icedtea_src_configure() {
    local myconf=(
        --enable-nss
        --enable-optimizations
        --enable-system-kerberos
        --enable-system-lcms
        --disable-system-pcsc
        --disable-system-sctp
# Having a bootstrap option but hard-disabling bootstrapping may look strange but
# we're using a pre-compiled icedtea for bootstrapping whereas *this* bootstrap
# switch is meant for using gcc[java]/gcj-jdk, iow: classpath, for bootstrapping
# which we don't support anymore as it was a nightmare.
        --disable-bootstrap
        --disable-hg
        --with-hotspot-src-zip="${FETCHEDDIR}"/hotspot-${PV}.tar.${ARC_EXT}
        --with-corba-src-zip="${FETCHEDDIR}"/corba-${PV}.tar.${ARC_EXT}
        --with-jaxp-src-zip="${FETCHEDDIR}"/jaxp-${PV}.tar.${ARC_EXT}
        --with-jaxws-src-zip="${FETCHEDDIR}"/jaxws-${PV}.tar.${ARC_EXT}
        --with-jdk-src-zip="${FETCHEDDIR}"/jdk-${PV}.tar.${ARC_EXT}
        --with-langtools-src-zip="${FETCHEDDIR}"/langtools-${PV}.tar.${ARC_EXT}
        --with-openjdk-src-zip="${FETCHEDDIR}"/openjdk-${SLOT}-${PV}.tar.${ARC_EXT}
        --with-parallel-jobs=${EXJOBS:-1}
        --with-pkgversion="Exherbo"
        --without-gcj
    )

    if ever at_least 3.0 ; then
        myconf+=(
            --with-nashorn-src-zip="${FETCHEDDIR}"/nashorn-${PV}.tar.${ARC_EXT}
        )
    fi

    # We used to unset the Java variables here. This can cause build failures, though.
    # Instead, we're only doing it now during bootstrapping during which no existing
    # JDK should be used.
    # We do NOT export the Java variables here in case the user knows what he's
    # doing and has modified them.
    export ANT_RESPECT_JAVA_HOME=TRUE

    if [[ -n ${MUST_BOOTSTRAP} ]] ; then
        # Unset the Java variables in order to make sure no existing JDK gets picked
        # up. We DO have to set JAVAC explicitly, though, or ecj.jar (which we don't
        # usually provide) will be required.
        unset JAVA_HOME CLASSPATH JAVAC JAVACMD
        export ANT_HOME=${WORKBASE}/apache-ant-bin-${BOOTSTRAP_ANT_VERSION}/usr/share/ant
        export ANT=${WORKBASE}/apache-ant-bin-${BOOTSTRAP_ANT_VERSION}/usr/share/ant/bin/ant
        export JAVAC=${WORKBASE}/${PN}-${BOOTSTRAP_VERSION}/bin/javac
        export PATH=${WORKBASE}/${PN}-bin-${BOOTSTRAP_VERSION}/bin:${PATH}
        myconf+=(
            --with-jdk-home=${WORKBASE}/icedtea7-${BOOTSTRAP_VERSION}
            --with-javac=${WORKBASE}/icedtea7-${BOOTSTRAP_VERSION}/bin/javac
            --with-javah=${WORKBASE}/icedtea7-${BOOTSTRAP_VERSION}/bin/javah
            --with-jar=${WORKBASE}/icedtea7-${BOOTSTRAP_VERSION}/bin/jar
            --with-rmic=${WORKBASE}/icedtea7-${BOOTSTRAP_VERSION}/bin/rmic
        )
    else
        if has_version dev-lang/icedtea8 ; then
            myconf+=( --with-jdk-home=/usr/$(exhost --target)/lib/icedtea8 )
        fi
    fi

    myconf+=( $(_icedtea_option doc docs) )

    # For some reason, icedtea really hates -march=native on x86.
    option platform:x86 && filter-flags -march=native

    econf ${myconf[@]}
}

icedtea_src_compile() {
    unset JAVA_HOME CLASSPATH JAVAC JAVACMD

    # unset _JAVA_OPTIONS to avoid failures due to Java spouting crap:
    #
    # configure: Found potential Boot JDK using configure arguments
    # configure: Potential Boot JDK found at /var/tmp/paludis/build/dev-lang-icedtea8-3.0.1/work/icedtea-3.0.1/bootstrap/jdk1.7.0 is incorrect JDK version (Picked up _JAVA_OPTIONS: -Dawt.useSystemAAFontSettings=lcd -Dswing.aatext=true); ignoring
    # configure: (Your Boot JDK must be version 7 or 8)
    # configure: error: The path given by --with-boot-jdk does not contain a valid Boot JDK
    unset _JAVA_OPTIONS

    # The build system needs this dir but doesn't create it.
    edo mkdir -p "${WORK}"/bootstrap/boot/jre/lib

    # The icedtea build system uses the OPTIONS variable for setting defines.
    env -u OPTIONS emake DISTRIBUTION_PATCHES="${DISTRIBUTION_PATCHES[@]}" \
        ALT_OBJCOPY="/usr/$(exhost --target)/bin/$(exhost --tool-prefix)objcopy" \
        CXX=${CXX} NM=${NM} AR=${AR} READELF=$(exhost --tool-prefix)readelf \
        DISABLE_HOTSPOT_OS_VERSION_CHECK=ok
}

icedtea_src_test_expensive() {
    # If someone wants to work on this: There are literally thousands of tests
    # which makes this very, very expensive in terms of time.
    # A few hundred of those need a running X server. Some fixable sandbox
    # violations due to binding to random ports on 127.0.0.1 occur.

    return

    emake check
}

icedtea_src_install() {
    # The target directory has changed several times already, so let's define it here.
    local build_target="${WORK}"/openjdk.build

    # Use ${PN} in java_home rather than ${PNV} or ${PN}-${SLOT} since the
    # name itself is implicitly slotted (the 6 in icedtea7 is equivalent in
    # meaning to SLOT=1.6, since JDK 6 is Java 1.6) and because the build
    # system expects to find an existing installation in /usr/lib*/icedtea7
    # when not bootstrapping with ecj.
    local java_home="/usr/$(exhost --target)/lib/${PN}"

    dodir "${java_home}"

    # There's no install target so we have to enter the build dir and install
    # everything manually.
    edo cd "${build_target}"
    edo cd "${build_target}"/images/j2sdk-image

    # The minimal set of standard docs.
    dodoc ASSEMBLY_EXCEPTION THIRD_PARTY_README

    # Install the JDK/JRE itself.
    insinto "${java_home}"
    doins -r bin include jre lib man

    # Install the demos and samples if requested.
    if option examples ; then
        doins -r demo
        doins -r sample
    fi

    # Fix the executables' permissions.
    edo chmod 755 "${IMAGE}/${java_home}"/{,jre/}bin/*

    # Link the Java KeyStore file generated by ca-certificates
    edo rm "${IMAGE}/${java_home}"/jre/lib/security/cacerts
    dosym /etc/pki/ca-trust/extracted/java/cacerts "${java_home}"/jre/lib/security/cacerts

    linkify_redundant_jdk_bins
    do_jdk_alternatives
}

icedtea_pkg_preinst() {
    if [[ -L ${ROOT}/usr/$(exhost --target)/${PN} ]]; then
        nonfatal edo rm "${ROOT}"/usr/$(exhost --target)/lib/${PN} || eerror "rm failed"
    fi
}

