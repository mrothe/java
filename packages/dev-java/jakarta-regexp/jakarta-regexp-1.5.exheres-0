# Copyright 2009 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

S_PN="${PN/jakarta-}"

require ant java

SUMMARY="A 100% Pure Java regular expression library"
DESCRIPTION="
Jakarta Regexp is a 100% pure Java regular expression package. Expression syntax
supports all of Perl5 syntax, with the exception of back reference substitution
markers (several people have mentioned this lack, so please feel free to
contribute a solution). It also supports a very limited subset of POSIX features.
Performance and features are somewhat inferior to ORO, and the new JDK 1.4
(java.util.regex) package, but the package is especially small and features the
ability to precompile expressions (without using object serialization) into byte
arrays, thus avoiding the need to load the expression compiler.
"
HOMEPAGE="http://jakarta.apache.org/${S_PN}/"
DOWNLOADS="mirror://apache/dist/jakarta/${S_PN}/source/${PNV}.tar.gz"

BUGS_TO="philantrop@exherbo.org"
REMOTE_IDS="freecode:apachejakartaregexp"

UPSTREAM_CHANGELOG="http://jakarta.apache.org/${S_PN}/changes.html"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES=""

ANT_SRC_COMPILE_PARAMS=( jar )
ANT_SRC_TEST_PARAMS=( test )

src_prepare() {
    edo rm *.jar
}

src_install() {
    ant_src_install

    dodir /usr/share/${PN}
    insinto /usr/share/${PN}
    doins ${ANT_BUILD_DIR}/${PNV}.jar
}

